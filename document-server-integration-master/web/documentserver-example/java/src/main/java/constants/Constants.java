package constants;

public class Constants {

    public static final String FILENAME_ALL_LOWERCASE = "filename";
    public static final String FILE_NAME = "fileName";
    public static final String FILE_EXT = "fileExt";
    public static final String EDITOR_MODE = "mode";
    public static final String FILE_MODEL = "file";
    public static final String TYPE = "type";
    public static final String DESKTOP = "desktop";
    public static final String EMBEDDED = "embedded";
    public static final String EDITOR_PAGE = "Editor page";
    public static final String ERROR = "error";
    public static final String DOC_SERVICE_API_URL = "docserviceApiUrl";
    public static final String UPLOAD = "upload";
    public static final String CONVERT = "convert";
    public static final String TRACK = "track";
    public static final String TEXT_PLAIN = "text/plain";
    public static final String HEADER_CONTENT_DISPOSITION = "content-disposition";
    public static final String USER_ADDRESS = "userAddress";
    public static final String URL = "url";
    public static final String HANDLER = "Handler";
    public static final String END_CONVERT = "endConvert";
    public static final String FILE_URL = "fileUrl";
    public static final String PERCENT = "percent";

    public static final String REGEX_HOST_ADDRESS = "[^0-9a-zA-Z.=]";

    public static final String EDITOR_JSP = "editor.jsp";
    public static final String INDEX_JSP = "index.jsp";

    public static final String EMPTY = "";
    public static final String POINT = ".";
    public static final String COLON = ":";
    public static final String EQUAL = "=";
    public static final String AMPERSAND = "&";
    public static final String UNDERSCORE = "_";
    public static final String SEMICOLON = ";";
    public static final String DOUBLE_QUOTES = "\"";
    public static final String SCANNER_DELIMITER = "\\A";
    public static final String PROPERTIES_DIVISOR = "\\|";
    public static final String WINDOWS_PATH_DIVISOR = "\\";
    public static final String PATH_DIVISOR = "/";
    public static final String CLOSE_PARENTESIS = ")";
    public static final String OPEN_PARENTESIS_WITH_SPACE = " (";
    public static final String HTTP_HOST_DIVIDER = "://";

    public static final String FILES_DOC_SERVICE_URL_API = "files.docservice.url.api";
    public static final String FILE_SIZE_MAX = "filesize-max";
    public static final String STORAGE_FOLDER = "storage-folder";
    public static final String FILES_DOC_SERVICE_VIEWED_DOCS = "files.docservice.viewed-docs";
    public static final String FILES_DOC_SERVICE_EDITED_DOCS = "files.docservice.edited-docs";
    public static final String FILES_DOC_SERVICE_CONVERT_DOCS = "files.docservice.convert-docs";
    public static final String FILES_DOC_SERVICE_URL_TEMP_STORAGE = "files.docservice.url.tempstorage";
    public static final String FILES_DOC_SERVICE_URL_CONVERTER = "files.docservice.url.converter";
    public static final String FILES_DOC_SERVICE_TIMEOUT = "files.docservice.timeout";

    public static final String OS_NAME = "os.name";

    public static final String DOCX_EXT =  ".docx";
    public static final String XLSX_EXT = ".xlsx";
    public static final String PPTX_EXT = ".pptx";

    public static final int MAX_LENGTH_ACCEPTED_KEY = 20;
    public static final long PERCENT_100 = 100l;

}
