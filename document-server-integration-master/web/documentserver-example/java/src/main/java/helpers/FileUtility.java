/*
 *
 * (c) Copyright Ascensio System Limited 2010-2018
 *
 * The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */


package helpers;

import constants.Constants;
import entities.FileType;

import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileUtility {
    static {
    }

    public static FileType getFileType(final String fileName) {
        final String ext = getFileExtension(fileName).toLowerCase();

        if (ExtsDocument.contains(ext))
            return FileType.Text;

        if (ExtsSpreadsheet.contains(ext))
            return FileType.Spreadsheet;

        if (ExtsPresentation.contains(ext))
            return FileType.Presentation;

        return FileType.Text;
    }

    public static List<String> ExtsDocument = Arrays.asList
            (
                    ".doc", ".docx", ".docm",
                    ".dot", ".dotx", ".dotm",
                    ".odt", ".fodt", ".ott", ".rtf", ".txt",
                    ".html", ".htm", ".mht",
                    ".pdf", ".djvu", ".fb2", ".epub", ".xps"
            );

    public static List<String> ExtsSpreadsheet = Arrays.asList
            (
                    ".xls", ".xlsx", ".xlsm",
                    ".xlt", ".xltx", ".xltm",
                    ".ods", ".fods", ".ots", ".csv"
            );

    public static List<String> ExtsPresentation = Arrays.asList
            (
                    ".pps", ".ppsx", ".ppsm",
                    ".ppt", ".pptx", ".pptm",
                    ".pot", ".potx", ".potm",
                    ".odp", ".fodp", ".otp"
            );


    public static String getFileName(final String url) {
        if (url == null) return null;

        //for external file url
        final String tempstorage = ConfigManager.getProperty(Constants.FILES_DOC_SERVICE_URL_TEMP_STORAGE);
        if (!tempstorage.isEmpty() && url.startsWith(tempstorage)) {
            final Map<String, String> params = getUrlParams(url);
            return params == null ? null : params.get(Constants.FILENAME_ALL_LOWERCASE);
        }

        final String fileName = url.substring(url.lastIndexOf(Constants.PATH_DIVISOR) + 1, url.length());
        return fileName;
    }

    public static String getFileNameWithoutExtension(final String url) {
        final String fileName = getFileName(url);
        if (fileName == null) return null;
        final String fileNameWithoutExt = fileName.substring(0, fileName.lastIndexOf(Constants.POINT));
        return fileNameWithoutExt;
    }

    public static String getFileExtension(final String url) {
        final String fileName = getFileName(url);
        if (fileName == null) return null;
        final String fileExt = fileName.substring(fileName.lastIndexOf(Constants.POINT));
        return fileExt.toLowerCase();
    }

    public static Map<String, String> getUrlParams(final String url) {
        try {
            final String query = new URL(url).getQuery();
            final String[] params = query.split(Constants.AMPERSAND);
            final Map<String, String> map = new HashMap<>();
            for (String param : params) {
                final String name = param.split(Constants.EQUAL)[0];
                final String value = param.split(Constants.EQUAL)[1];
                map.put(name, value);
            }
            return map;
        } catch (Exception ex) {
            return null;
        }
    }
}
