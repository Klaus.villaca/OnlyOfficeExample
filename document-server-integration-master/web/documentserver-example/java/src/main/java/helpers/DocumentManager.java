/*
 *
 * (c) Copyright Ascensio System Limited 2010-2018
 *
 * The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package helpers;

import java.io.*;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import constants.Constants;
import entities.FileType;


public class DocumentManager {
    private static String OS = System.getProperty(Constants.OS_NAME).toLowerCase();
    private static String storagePath = ConfigManager.getProperty(Constants.STORAGE_FOLDER);

    private static HttpServletRequest request;

    public static void init(HttpServletRequest req, HttpServletResponse resp) {
        request = req;
    }

    public static long getMaxFileSize() {
        long size;

        try {
            size = Long.parseLong(ConfigManager.getProperty(Constants.FILE_SIZE_MAX));
        } catch (Exception ex) {
            size = 0;
        }

        final long defaultSize = 5l * 1024l * 1024l;
        final long toReturn = size > 0 ? size : defaultSize;
        return toReturn;
    }

    public static List<String> getFileExts() {
        final List<String> res = new ArrayList<>();
        res.addAll(getViewedExts());
        res.addAll(getEditedExts());
        res.addAll(getConvertExts());
        return res;
    }

    public static List<String> getViewedExts() {
        final String exts = ConfigManager.getProperty(Constants.FILES_DOC_SERVICE_VIEWED_DOCS);
        return Arrays.asList(exts.split(Constants.PROPERTIES_DIVISOR));
    }

    public static List<String> getEditedExts() {
        final String exts = ConfigManager.getProperty(Constants.FILES_DOC_SERVICE_EDITED_DOCS);
        return Arrays.asList(exts.split(Constants.PROPERTIES_DIVISOR));
    }

    public static List<String> getConvertExts() {
        final String exts = ConfigManager.getProperty(Constants.FILES_DOC_SERVICE_CONVERT_DOCS);
        return Arrays.asList(exts.split(Constants.PROPERTIES_DIVISOR));
    }

    public static String curUserHostAddress(String userAddress) {
        if (userAddress == null) {
            try {
                userAddress = InetAddress.getLocalHost().getHostAddress();
            } catch (Exception ex) {
                userAddress = Constants.EMPTY;
            }
        }
        final String toReturn = userAddress.replaceAll(Constants.REGEX_HOST_ADDRESS, Constants.UNDERSCORE);
        return toReturn;
    }

    public static String storagePath(final String fileName, final String userAddress) {
        final String serverPath = request.getSession().getServletContext().getRealPath(Constants.EMPTY);
        final String hostAddress;
        if (userAddress == null || userAddress.length() == 0) {
            hostAddress = curUserHostAddress(userAddress);
        } else {
            hostAddress = userAddress;
        }

        StringBuilder directory = new StringBuilder();
        if (isWindows()) {
            directory.append(serverPath).append(Constants.WINDOWS_PATH_DIVISOR)
                    .append(storagePath).append(Constants.WINDOWS_PATH_DIVISOR);
        } else {
            directory.append(serverPath).append(storagePath).append(Constants.PATH_DIVISOR);
        }
        System.out.println("storagePath - First File: " + directory.toString());

        File file = new File(directory.toString());
        if (!file.exists()) {
            boolean isSucess = createDirectory(file);
            System.out.println("First creation isSuccess: " + isSucess);
        }

        if (isWindows()) {
            if (hostAddress.startsWith(storagePath)) {
                directory = new StringBuilder(serverPath).append(Constants.WINDOWS_PATH_DIVISOR)
                        .append(hostAddress).append(Constants.WINDOWS_PATH_DIVISOR);
            } else {
                directory.append(hostAddress).append(Constants.WINDOWS_PATH_DIVISOR);
            }
        } else {
            if (hostAddress.startsWith(storagePath)) {
                directory = new StringBuilder(serverPath).append(hostAddress).append(Constants.PATH_DIVISOR);
            } else {
                directory.append(hostAddress).append(Constants.PATH_DIVISOR);
            }
        }

        System.out.println("storagePath - First second: " + directory);
        file = new File(directory.toString());
        if (!file.exists()) {
            boolean isSucess = createDirectory(file);
            System.out.println("Second creation isSuccess: " + isSucess);
        }

        file = null;
        final String toReturn = directory + fileName;
        return toReturn;
    }

    public static String getCorrectName(final String fileName) {
        final String baseName = FileUtility.getFileNameWithoutExtension(fileName);
        final String ext = FileUtility.getFileExtension(fileName);
        StringBuilder name = new StringBuilder(baseName).append(ext);
        final String storagePath = storagePath(name.toString(), null);
        File file = new File(storagePath);
        for (int i = 1; file.exists(); i++) {
            name = new StringBuilder(baseName).append(Constants.UNDERSCORE)
                    .append(i).append(ext);
            final String tempPath = storagePath(name.toString(), null);
            file = new File(tempPath);
        }

        return name.toString();
    }

    public static String createDemo(final String fileExt) throws Exception {
        final String demoName = "sample." + fileExt;
        final String fileName = getCorrectName(demoName);

        final InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(demoName);
        final String tempStoragePath = storagePath(fileName, null);
        final File file = new File(tempStoragePath);
        FileOutputStream out = null;

        try {
            out = new FileOutputStream(file);
            int read;
            final byte[] bytes = new byte[4096];
            while ((read = stream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
                out = null;
            }
        }

        return fileName;
    }





    public static String getFileUri(final String fileName) throws Exception {
//        final String serverPath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
//        final StringBuilder serverPath = new StringBuilder(request.getScheme())
//                .append(Constants.HTTP_HOST_DIVIDER).append(request.getServerName()).append(Constants.COLON)
//                .append(request.getServerPort()).append(request.getContextPath());
//
//        final String hostAddress = curUserHostAddress(null);
//
//        final StringBuilder filePath = new StringBuilder(serverPath).append(Constants.PATH_DIVISOR)
//                .append(storagePath).append(Constants.PATH_DIVISOR)
//                .append(hostAddress).append(Constants.PATH_DIVISOR)
//                .append(fileName);
//        System.out.println("getFileUri: " + filePath);

        try {
            String serverPath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
            String hostAddress = curUserHostAddress(null);
//            String storagePath = StoragePath(fileName, hostAddress).replace(fileName, "");

            String filePath = serverPath + "/" + storagePath + "/" + hostAddress + "/" + URLEncoder.encode(fileName, java.nio.charset.StandardCharsets.UTF_8.toString());
//            String filePath = serverPath + storagePath + URLEncoder.encode(fileName, java.nio.charset.StandardCharsets.UTF_8.toString());
            System.out.println("GetFileUri: " + filePath);
            return filePath;
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError("UTF-8 is unknown");
        }
//        return filePath.toString();
    }

    public static String getServerUrl() {
        final StringBuilder toReturn = new StringBuilder(request.getScheme()).append(Constants.HTTP_HOST_DIVIDER)
                .append(request.getServerName()).append(Constants.COLON)
                .append(request.getServerPort());
        System.out.println("getServerUrl: " + toReturn.toString());
        return toReturn.toString();
    }

    public static String getCallback(final String fileName) {
//        final String userIP = curUserHostAddress(null);
//        final StringBuilder serverPath = new StringBuilder(request.getScheme()).append(Constants.HTTP_HOST_DIVIDER)
//                .append(request.getServerName()).append(Constants.COLON)
//                .append(request.getServerPort()).append(request.getContextPath());
//        try {
////            final String query = "?type=track&fileName=" +
////                    URLEncoder.encode(fileName, java.nio.charset.StandardCharsets.UTF_8.toString()) +
////                    "&userAddress=" +
////                    URLEncoder.encode(storagePath + "/" + userIP, java.nio.charset.StandardCharsets.UTF_8.toString());
//            final String query = "?type=track&fileName=" +
//                    URLEncoder.encode(fileName, java.nio.charset.StandardCharsets.UTF_8.toString()) +
//                    "&userAddress=" +
//                    storagePath + "/" + userIP;
//            final String toReturn = serverPath.toString() + "/IndexServlet" + query;
//            System.out.println("getCallback: " + toReturn);
//            return toReturn;
//        } catch (UnsupportedEncodingException e) {
//            throw new AssertionError("UTF-8 is unknown");
//        }


        String serverPath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        String hostAddress = curUserHostAddress(null);
//        String storagePath = StoragePath(fileName, hostAddress).replace(fileName, "");
        try {
            String query = "?type=track&fileName=" +
                    URLEncoder.encode(fileName, java.nio.charset.StandardCharsets.UTF_8.toString()) +
                    "&userAddress=" +
                    URLEncoder.encode(storagePath + "/" + hostAddress + "/", java.nio.charset.StandardCharsets.UTF_8.toString());
//                    URLEncoder.encode(storagePath + "/" + hostAddress, java.nio.charset.StandardCharsets.UTF_8.toString());
            final String toReturn = serverPath + "/IndexServlet" + query;
            System.out.println("GetCallback: " + toReturn);
            return toReturn;
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError("UTF-8 is unknown");
        }

    }

    public static String getInternalExtension(final FileType fileType) {
        final String toReturn;
        switch (fileType) {
            case Text:
                toReturn = Constants.DOCX_EXT;
                break;

            case Spreadsheet:
                toReturn = Constants.XLSX_EXT;
                break;

            case Presentation:
                toReturn = Constants.PPTX_EXT;
                break;

            default:
                toReturn = Constants.DOCX_EXT;
        }
        return toReturn;
    }


    private static boolean createDirectory(final File file) {
        boolean isSucess = false;
        if (isWindows()) {
            isSucess = file.mkdir();
        } else {
            try {
                final Set<PosixFilePermission> permissions = getPosixFilePermissions();
                Files.createDirectory(file.toPath(), PosixFilePermissions.asFileAttribute(permissions));
                isSucess = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        System.out.println("isSuccess: " + isSucess);
        return isSucess;
    }

    private static Set<PosixFilePermission> getPosixFilePermissions() {
        final Set<PosixFilePermission> perms = new HashSet<PosixFilePermission>();
        //add owners permission
        perms.add(PosixFilePermission.OWNER_READ);
        perms.add(PosixFilePermission.OWNER_WRITE);
        perms.add(PosixFilePermission.OWNER_EXECUTE);
        //add group permissions
        perms.add(PosixFilePermission.GROUP_READ);
        perms.add(PosixFilePermission.GROUP_WRITE);
        perms.add(PosixFilePermission.GROUP_EXECUTE);
        //add others permissions
        perms.add(PosixFilePermission.OTHERS_READ);
        perms.add(PosixFilePermission.OTHERS_WRITE);
        perms.add(PosixFilePermission.OTHERS_EXECUTE);
        return perms;
    }

    private static boolean isWindows() {
        return (OS.indexOf("win") >= 0);
    }
}