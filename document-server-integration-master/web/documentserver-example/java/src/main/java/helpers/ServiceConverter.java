/*
 *
 * (c) Copyright Ascensio System Limited 2010-2018
 *
 * The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package helpers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.UUID;

import constants.Constants;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class ServiceConverter {
    private static int ConvertTimeout = 120000;
    private static final String DocumentConverterUrl = ConfigManager.getProperty(Constants.FILES_DOC_SERVICE_URL_CONVERTER);
    private static final MessageFormat ConvertParams = new MessageFormat("?url={0}&outputtype={1}&filetype={2}&title={3}&key={4}");

    static {
        try {
            int timeout = Integer.parseInt(ConfigManager.getProperty(Constants.FILES_DOC_SERVICE_TIMEOUT));
            if (timeout > 0) {
                ConvertTimeout = timeout;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static String getConvertedUri(final String documentUri, String fromExtension,
                                         final String toExtension, String documentRevisionId,
                                         Boolean isAsync) throws Exception {
        fromExtension = (fromExtension == null || fromExtension.isEmpty()) ? FileUtility.getFileExtension(documentUri) : fromExtension;

        String title = FileUtility.getFileName(documentUri);
        title = (title == null || title.isEmpty()) ? UUID.randomUUID().toString() : title;

        documentRevisionId = documentRevisionId == null || documentRevisionId.isEmpty() ? documentUri : documentRevisionId;
        documentRevisionId = generateRevisionId(documentRevisionId);

        final Object[] args = {
                URLEncoder.encode(documentUri, java.nio.charset.StandardCharsets.UTF_8.toString()),
                toExtension.replace(Constants.POINT, Constants.EMPTY),
                fromExtension.replace(Constants.POINT, Constants.EMPTY),
                title,
                documentRevisionId
        };

        final StringBuilder urlToConverter = new StringBuilder(DocumentConverterUrl).append(ConvertParams.format(args));

        if (isAsync)
            urlToConverter.append("&async=true");

        final URL url = new URL(urlToConverter.toString());
        final java.net.HttpURLConnection connection = (java.net.HttpURLConnection) url.openConnection();
        connection.setRequestProperty("Accept", "application/json");
        connection.setConnectTimeout(ConvertTimeout);

        final InputStream stream = connection.getInputStream();
        if (stream == null)
            throw new Exception("Could not get an answer");

        final String jsonString = convertStreamToString(stream);
        connection.disconnect();

        final String toReturn = getResponseUri(jsonString);
        return toReturn;
    }

    public static String generateRevisionId(String expectedKey) {
        final String REGEX = "[^0-9-.a-zA-Z_=]";
        if (expectedKey.length() > Constants.MAX_LENGTH_ACCEPTED_KEY)
            expectedKey = Integer.toString(expectedKey.hashCode());

        final String key = expectedKey.replace(REGEX, Constants.UNDERSCORE);

        return key.substring(0, Math.min(key.length(), Constants.MAX_LENGTH_ACCEPTED_KEY));
    }

    private static void processConvertServiceResponceError(int errorCode) throws Exception {
        final StringBuilder errorMessage = new StringBuilder(Constants.EMPTY);
        final String errorMessageTemplate = "Error occurred in the ConvertService: ";

        switch (errorCode) {
            case -8:
                errorMessage.append(errorMessageTemplate).append("Error document VKey");
                break;
            case -7:
                errorMessage.append(errorMessageTemplate).append("Error document request");
                break;
            case -6:
                errorMessage.append(errorMessageTemplate).append("Error database");
                break;
            case -5:
                errorMessage.append(errorMessageTemplate).append("Error unexpected guid");
                break;
            case -4:
                errorMessage.append(errorMessageTemplate).append("Error download error");
                break;
            case -3:
                errorMessage.append(errorMessageTemplate).append("Error convertation error");
                break;
            case -2:
                errorMessage.append(errorMessageTemplate).append("Error convertation timeout");
                break;
            case -1:
                errorMessage.append(errorMessageTemplate).append("Error convertation unknown");
                break;
            case 0:
                break;
            default:
                errorMessage.append("ErrorCode = ").append(errorCode);
                break;
        }

        throw new Exception(errorMessage.toString());
    }

    private static String getResponseUri(final String jsonString) throws Exception {
        final JSONObject jsonObj = convertStringToJSON(jsonString);

        final String error = (String) jsonObj.get(Constants.ERROR);
        if (error != null && error != Constants.EMPTY)
            processConvertServiceResponceError(Integer.parseInt(error));

        final Boolean isEndConvert = (Boolean) jsonObj.get(Constants.END_CONVERT);

        Long resultPercent = 0l;
        String responseUri = null;

        if (isEndConvert) {
            resultPercent = Constants.PERCENT_100;
            responseUri = (String) jsonObj.get(Constants.FILE_URL);
        } else {
            resultPercent = (Long) jsonObj.get(Constants.PERCENT);
            resultPercent = (resultPercent >= Constants.PERCENT_100)? 99l: resultPercent;
        }

        final String toReturn = (resultPercent >= Constants.PERCENT_100)? responseUri: Constants.EMPTY;
        return toReturn;
    }

    private static String convertStreamToString(final InputStream stream) throws IOException {
        final InputStreamReader inputStreamReader = new InputStreamReader(stream);
        final StringBuilder stringBuilder = new StringBuilder(Constants.EMPTY);
        final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String line = bufferedReader.readLine();

        while (line != null) {
            stringBuilder.append(line);
            line = bufferedReader.readLine();
        }

        final String result = stringBuilder.toString();
        return result;
    }

    private static JSONObject convertStringToJSON(final String jsonString) throws ParseException {
        final JSONParser parser = new JSONParser();
        final Object obj = parser.parse(jsonString);
        final JSONObject jsonObj = (JSONObject) obj;
        return jsonObj;
    }
}