/*
 *
 * (c) Copyright Ascensio System Limited 2010-2018
 *
 * The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */


package controllers;

import constants.Constants;
import helpers.ConfigManager;
import helpers.DocumentManager;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entities.FileModel;


@WebServlet(name = "EditorServlet", urlPatterns = {"/EditorServlet"})
public class EditorServlet extends HttpServlet {

    protected void processRequest(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        String fileName = Constants.EMPTY;
        if (request.getParameterMap().containsKey(Constants.FILE_NAME)) {
            fileName = request.getParameter(Constants.FILE_NAME);
        }

        String fileExt = null;
        if (request.getParameterMap().containsKey(Constants.FILE_EXT)) {
            fileExt = request.getParameter(Constants.FILE_EXT);
        }

        if (fileExt != null) {
            try {
                DocumentManager.init(request, response);
                fileName = DocumentManager.createDemo(fileExt);
            } catch (Exception ex) {
                final StringBuilder msg = new StringBuilder(Constants.ERROR)
                        .append(Constants.COLON).append(ex.getMessage());
                response.getWriter().write(msg.toString());
            }
        }

        String mode = Constants.EMPTY;
        if (request.getParameterMap().containsKey(Constants.EDITOR_MODE)) {
            mode = request.getParameter(Constants.EDITOR_MODE);
        }
        final Boolean desktopMode = !Constants.EMBEDDED.equals(mode);

        final FileModel file = new FileModel();
        file.SetTypeDesktop(desktopMode);
        file.SetFileName(fileName);

        request.setAttribute(Constants.FILE_MODEL, file);
        request.setAttribute(Constants.EDITOR_MODE, mode);
        request.setAttribute(Constants.TYPE, desktopMode ? Constants.DESKTOP : Constants.EMBEDDED);
        request.setAttribute(Constants.DOC_SERVICE_API_URL, ConfigManager.getProperty(Constants.FILES_DOC_SERVICE_URL_API));
        request.getRequestDispatcher(Constants.EDITOR_JSP).forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return Constants.EDITOR_PAGE;
    }
}
