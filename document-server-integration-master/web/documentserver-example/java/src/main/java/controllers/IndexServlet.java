/*
 *
 * (c) Copyright Ascensio System Limited 2010-2018
 *
 * The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */


package controllers;

import constants.Constants;
import helpers.DocumentManager;
import helpers.ServiceConverter;

import java.io.*;
import java.net.URL;
import java.util.Scanner;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import entities.FileType;
import helpers.FileUtility;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

@WebServlet(name = "IndexServlet", urlPatterns = {"/IndexServlet"})
@MultipartConfig
public class IndexServlet extends HttpServlet {
    protected void processRequest(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final String action = request.getParameter(Constants.TYPE);

        if (action == null) {
            request.getRequestDispatcher(Constants.INDEX_JSP).forward(request, response);
            return;
        }

        DocumentManager.init(request, response);
        final PrintWriter writer = response.getWriter();

        switch (action.toLowerCase()) {
            case Constants.UPLOAD:
                upload(request, response, writer);
                break;
            case Constants.CONVERT:
                convert(request, response, writer);
                break;
            case Constants.TRACK:
                track(request, response, writer);
                break;
        }
    }


    private static void upload(final HttpServletRequest request, final HttpServletResponse response, final PrintWriter writer) {
        response.setContentType(Constants.TEXT_PLAIN);
        try {
            final Part httpPostedFile = request.getPart(Constants.FILE_MODEL);
            final String[] httpPostHeader = httpPostedFile.getHeader(Constants.HEADER_CONTENT_DISPOSITION).split(Constants.SEMICOLON);
            String fileName = Constants.EMPTY;
            for (String content : httpPostHeader) {
                if (content.trim().startsWith(Constants.FILENAME_ALL_LOWERCASE)) {
                    fileName = content.substring(content.indexOf(Constants.EQUAL) + 1).trim()
                            .replace(Constants.DOUBLE_QUOTES, Constants.EMPTY);
                }
            }

            final long curSize = httpPostedFile.getSize();
            if (DocumentManager.getMaxFileSize() < curSize || curSize <= 0) {
                writer.write("{ \"error\": \"File size is incorrect\"}");
                return;
            }

            final String curExt = FileUtility.getFileExtension(fileName);
            if (!DocumentManager.getFileExts().contains(curExt)) {
                writer.write("{ \"error\": \"File type is not supported\"}");
                return;
            }

            final InputStream fileStream = httpPostedFile.getInputStream();

            fileName = DocumentManager.getCorrectName(fileName);
            String fileStoragePath = DocumentManager.storagePath(fileName, null);

            final File file = new File(fileStoragePath);
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(file);
                int read;
                final byte[] bytes = new byte[4096];
                while ((read = fileStream.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }

                out.flush();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (out != null) {
                    out.close();
                    out = null;
                }
            }

            writer.write("{ \"filename\": \"" + fileName + "\"}");

        } catch (IOException | ServletException e) {
            writer.write("{ \"error\": \"" + e.getMessage() + "\"}");
        }
    }

    private static void convert(final HttpServletRequest request, final HttpServletResponse response, final PrintWriter writer) {
        response.setContentType(Constants.TEXT_PLAIN);

        try {
            String fileName = request.getParameter(Constants.FILENAME_ALL_LOWERCASE);
            final String fileUri = DocumentManager.getFileUri(fileName);
            final String fileExt = FileUtility.getFileExtension(fileName);
            final FileType fileType = FileUtility.getFileType(fileName);
            final String internalFileExt = DocumentManager.getInternalExtension(fileType);

            if (DocumentManager.getConvertExts().contains(fileExt)) {
                final String key = ServiceConverter.generateRevisionId(fileUri);
                final String newFileUri = ServiceConverter.getConvertedUri(fileUri, fileExt, internalFileExt, key, true);

                if (newFileUri == Constants.EMPTY) {
                    writer.write("{ \"step\" : \"0\", \"filename\" : \"" + fileName + "\"}");
                    return;
                }

                final String correctName = DocumentManager.getCorrectName(FileUtility.getFileNameWithoutExtension(fileName) + internalFileExt);

                final URL url = new URL(newFileUri);
                final java.net.HttpURLConnection connection = (java.net.HttpURLConnection) url.openConnection();
                final InputStream stream = connection.getInputStream();

                if (stream == null) {
                    throw new Exception("Stream is null");
                }

                final File convertedFile = new File(DocumentManager.storagePath(correctName, null));
                FileOutputStream out = null;
                try {
                    out = new FileOutputStream(convertedFile);
                    int read;
                    final byte[] bytes = new byte[4096];
                    while ((read = stream.read(bytes)) != -1) {
                        out.write(bytes, 0, read);
                    }

                    out.flush();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (out != null) {
                        out.close();
                        out = null;
                    }
                }

                connection.disconnect();

                //remove source file ?
                //File sourceFile = new File(DocumentManager.storagePath(fileName, null));
                //sourceFile.delete();

                fileName = correctName;
            }

            writer.write("{ \"filename\" : \"" + fileName + "\"}");

        } catch (Exception ex) {
            writer.write("{ \"error\": \"" + ex.getMessage() + "\"}");
        }
    }

    private static void track(final HttpServletRequest request, final HttpServletResponse response, final PrintWriter writer) {
        final String userAddress = request.getParameter(Constants.USER_ADDRESS);
        final String fileName = request.getParameter(Constants.FILE_NAME);
        final String storagePath = DocumentManager.storagePath(fileName, userAddress);
        String body = Constants.EMPTY;

        Scanner scanner = null;
        try {
            scanner = new Scanner(request.getInputStream());
            scanner.useDelimiter(Constants.SCANNER_DELIMITER);
            body = scanner.hasNext() ? scanner.next() : Constants.EMPTY;
            scanner.close();
        } catch (Exception ex) {
            writer.write("get request.getInputStream error:" + ex.getMessage());
            return;
        } finally {
            if (scanner != null) {
                scanner.close();
            }

        }

        if (body.isEmpty()) {
            writer.write("empty request.getInputStream");
            return;
        }

        final JSONParser parser = new JSONParser();
        JSONObject jsonObj;

        try {
            final Object obj = parser.parse(body.toString());
            jsonObj = (JSONObject) obj;
        } catch (Exception ex) {
            writer.write("JSONParser.parse error:" + ex.getMessage());
            return;
        }

        long status = (long) jsonObj.get("status");

        int saved = 0;
        if (status == 2 || status == 3)//MustSave, Corrupted
        {
            final String downloadUri = (String) jsonObj.get(Constants.URL);

            try {
                final URL url = new URL(downloadUri);
                final java.net.HttpURLConnection connection = (java.net.HttpURLConnection) url.openConnection();
                final InputStream stream = connection.getInputStream();

                if (stream == null) {
                    throw new Exception("Stream is null");
                }

                final File savedFile = new File(storagePath);
                FileOutputStream out = null;
                try {
                    out = new FileOutputStream(savedFile);
                    int read;
                    final byte[] bytes = new byte[4096];
                    while ((read = stream.read(bytes)) != -1) {
                        out.write(bytes, 0, read);
                    }

                    out.flush();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (out != null) {
                        out.close();
                        out = null;
                    }
                }

                connection.disconnect();

            } catch (Exception ex) {
                saved = 1;
            }
        }

        writer.write("{\"error\":" + saved + "}");
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return Constants.HANDLER;
    }
}
